package com.mastertech.cartao.models.mappers;

import com.mastertech.cartao.feignclients.ClienteFeignClient;
import com.mastertech.cartao.models.Cartao;
import com.mastertech.cartao.models.dtos.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CartaoMapper {

    @Autowired
    private ClienteFeignClient clienteFeignClient;

    public Cartao fromCreateRequest(CreateCartaoRequest cartaoCreateRequest) {
        Cartao cartao = new Cartao();
        cartao.setNumero(cartaoCreateRequest.getNumero());
        cartao.setClienteId(cartaoCreateRequest.getClienteId());
        return cartao;
    }

    public CreateCartaoResponse toCreateResponse(Cartao cartao) {
        CreateCartaoResponse createCartaoResponse = new CreateCartaoResponse();

        createCartaoResponse.setId(cartao.getId());
        createCartaoResponse.setNumero(cartao.getNumero());
        createCartaoResponse.setCliente(this.clienteFeignClient.getClienteById(cartao.getClienteId()));
        createCartaoResponse.setAtivo(cartao.getAtivo());

        return createCartaoResponse;
    }

    public Cartao fromUpdateRequest(UpdateCartaoRequest updateCartaoRequest) {
        Cartao cartao = new Cartao();
        cartao.setAtivo(updateCartaoRequest.getAtivo());
        return cartao;
    }

    public UpdateCartaoResponse toUpdateResponse(Cartao cartao) {
        UpdateCartaoResponse updateCartaoResponse = new UpdateCartaoResponse();

        updateCartaoResponse.setId(cartao.getId());
        updateCartaoResponse.setNumero(cartao.getNumero());
        updateCartaoResponse.setCliente(this.clienteFeignClient.getClienteById(cartao.getClienteId()));
        updateCartaoResponse.setAtivo(cartao.getAtivo());

        return updateCartaoResponse;
    }

    public GetCartaoResponse toGetResponse(Cartao cartao) {
        GetCartaoResponse getCartaoResponse = new GetCartaoResponse();

        getCartaoResponse.setId(cartao.getId());
        getCartaoResponse.setNumero(cartao.getNumero());
        getCartaoResponse.setCliente(this.clienteFeignClient.getClienteById(cartao.getClienteId()));

        return getCartaoResponse;
    }
}

