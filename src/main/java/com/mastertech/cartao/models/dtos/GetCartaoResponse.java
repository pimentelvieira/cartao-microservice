package com.mastertech.cartao.models.dtos;

import com.mastertech.cartao.feignclients.dtos.GetClienteResponse;

public class GetCartaoResponse {

    private Integer id;

    private String numero;

    private GetClienteResponse cliente;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public GetClienteResponse getCliente() {
        return cliente;
    }

    public void setCliente(GetClienteResponse cliente) {
        this.cliente = cliente;
    }
}

