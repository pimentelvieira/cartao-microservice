package com.mastertech.cartao.feignclients;

import com.mastertech.cartao.feignclients.configuration.ClienteFeignClientConfiguration;
import com.mastertech.cartao.feignclients.dtos.GetClienteResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "cliente-microservice", configuration = ClienteFeignClientConfiguration.class)
@RequestMapping("/cliente")
public interface ClienteFeignClient {

    @GetMapping("/{id}")
    public GetClienteResponse getClienteById(@PathVariable Integer id);
}
