package com.mastertech.cartao.feignclients.decoder;

import com.mastertech.cartao.exceptions.ClienteNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.http.HttpStatus;

public class ClienteFeignClientErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == HttpStatus.BAD_REQUEST.value()) {
            return new ClienteNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}

