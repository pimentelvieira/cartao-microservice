package com.mastertech.cartao.feignclients.configuration;

import com.mastertech.cartao.feignclients.decoder.ClienteFeignClientErrorDecoder;
import com.mastertech.cartao.feignclients.fallback.ClienteFeignClientFallback;
import feign.Feign;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteFeignClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ClienteFeignClientErrorDecoder();
    }

    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallbackFactory(ClienteFeignClientFallback::new)
                .build();

        return Resilience4jFeign.builder(decorators);
    }
}
