package com.mastertech.cartao.feignclients.fallback;

import com.mastertech.cartao.exceptions.ClienteMicroserviceOfflineException;
import com.mastertech.cartao.feignclients.ClienteFeignClient;
import com.mastertech.cartao.feignclients.dtos.GetClienteResponse;
import com.netflix.client.ClientException;

public class ClienteFeignClientFallback implements ClienteFeignClient {

    private Exception cause;

    public ClienteFeignClientFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public GetClienteResponse getClienteById(Integer id) {
        if(cause.getCause() instanceof ClientException) {
            throw new ClienteMicroserviceOfflineException();
        } else {
            throw (RuntimeException) cause;
        }
    }
}

