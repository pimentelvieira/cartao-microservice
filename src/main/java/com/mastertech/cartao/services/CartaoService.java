package com.mastertech.cartao.services;

import com.mastertech.cartao.exceptions.CartaoAlreadyExistsException;
import com.mastertech.cartao.exceptions.CartaoNotFoundException;
import com.mastertech.cartao.feignclients.ClienteFeignClient;
import com.mastertech.cartao.models.Cartao;
import com.mastertech.cartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteFeignClient clienteFeignClient;

    public Cartao create(Cartao cartao) {

        clienteFeignClient.getClienteById(cartao.getClienteId());

        Optional<Cartao> byNumero = cartaoRepository.findByNumero(cartao.getNumero());

        if(byNumero.isPresent()) {
            throw new CartaoAlreadyExistsException();
        }

        cartao.setAtivo(false);

        return cartaoRepository.save(cartao);
    }

    public Cartao update(Cartao updatedCartao) {
        Cartao databaseCartao = getByNumero(updatedCartao.getNumero());

        databaseCartao.setAtivo(updatedCartao.getAtivo());

        return cartaoRepository.save(databaseCartao);
    }

    public Cartao getById(Integer id) {
        Optional<Cartao> byId = cartaoRepository.findById(id);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

    public Cartao getByNumero(String numero) {
        Optional<Cartao> byId = cartaoRepository.findByNumero(numero);

        if(!byId.isPresent()) {
            throw new CartaoNotFoundException();
        }

        return byId.get();
    }

}

