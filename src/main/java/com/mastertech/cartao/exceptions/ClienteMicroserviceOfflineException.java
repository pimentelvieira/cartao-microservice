package com.mastertech.cartao.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "Microserviço de clientes offline")
public class ClienteMicroserviceOfflineException extends RuntimeException {
}

