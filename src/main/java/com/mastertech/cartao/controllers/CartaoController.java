package com.mastertech.cartao.controllers;

import com.mastertech.cartao.models.Cartao;
import com.mastertech.cartao.models.dtos.*;
import com.mastertech.cartao.models.mappers.CartaoMapper;
import com.mastertech.cartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper cartaoMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCartaoResponse create(@RequestBody CreateCartaoRequest createCartaoRequest) {

        Cartao cartao = cartaoMapper.fromCreateRequest(createCartaoRequest);

        cartao = cartaoService.create(cartao);

        return cartaoMapper.toCreateResponse(cartao);
    }

    @PatchMapping("/{numero}")
    public UpdateCartaoResponse update(@PathVariable String numero, @RequestBody UpdateCartaoRequest updateCartaoRequest) {

        Cartao cartao = cartaoMapper.fromUpdateRequest(updateCartaoRequest);
        cartao.setNumero(numero);

        cartao = cartaoService.update(cartao);
        return cartaoMapper.toUpdateResponse(cartao);
    }

    @GetMapping("/{numero}")
    public GetCartaoResponse getByNumero(@PathVariable String numero) {
        Cartao byNumero = cartaoService.getByNumero(numero);

        return cartaoMapper.toGetResponse(byNumero);
    }

    @GetMapping("/id/{id}")
    public GetCartaoResponse getById(@PathVariable Integer id) {
        Cartao byId = cartaoService.getById(id);

        return cartaoMapper.toGetResponse(byId);
    }
}